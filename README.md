# Daniel OS

## Working

- Interrupts
- Printing to screen
- Simple timer 
- Key event detection

## To be implemented

- Useful keyboard input
- strings.h (memcpy, memset, strcmp etc)
- paging
- mallard the helpful malloc duck.
- Terminal buffer (with scrolling)
- Ascii gui? (For no other reason than it being fun)

## Notes
vga/output needs some revision/ cleaning up.
