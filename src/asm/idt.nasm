[GLOBAL idt_flush]

idt_flush: 
	mov eax, [esp+4] ; Get the idt pointer off the stack (this is how it is passed as a param)
	lidt [eax] ; Load the idt.
	ret

