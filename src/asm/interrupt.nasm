;; Define a macro for creating all the 32 functions
;; Some interrupts send an error code, we need it to be consistent so any interrupts that dont send an error code will send a dummy one.

%macro ISR_NOERRORCODE 1 ; Takes one parameter
	[GLOBAL isr%1]
	isr%1:
		cli
		push byte 0 ; Push dummy error
		push byte %1
		jmp isr_common_stub ; Jump to common handler
%endmacro

%macro ISR_ERRORCODE 1
	[GLOBAL isr%1]
	isr%1:
		cli
		push byte %1 
		jmp isr_common_stub 
%endmacro

%macro IRQ 2
	[GLOBAL irq%1]
	irq%1:
		cli
		push byte 0
		push byte %2
		jmp irq_common_stub
%endmacro

ISR_NOERRORCODE 0
ISR_NOERRORCODE 1
ISR_NOERRORCODE 2
ISR_NOERRORCODE 3
ISR_NOERRORCODE 4
ISR_NOERRORCODE 5
ISR_NOERRORCODE 6
ISR_NOERRORCODE 7
ISR_ERRORCODE   8
ISR_NOERRORCODE 9
ISR_ERRORCODE   10
ISR_ERRORCODE   11
ISR_ERRORCODE   12
ISR_ERRORCODE   13
ISR_ERRORCODE   14
ISR_NOERRORCODE 15
ISR_NOERRORCODE 16
ISR_NOERRORCODE 17
ISR_NOERRORCODE 18
ISR_NOERRORCODE 19
ISR_NOERRORCODE 20
ISR_NOERRORCODE 21
ISR_NOERRORCODE 22
ISR_NOERRORCODE 23
ISR_NOERRORCODE 24
ISR_NOERRORCODE 25
ISR_NOERRORCODE 26
ISR_NOERRORCODE 27
ISR_NOERRORCODE 28
ISR_NOERRORCODE 29
ISR_NOERRORCODE 30
ISR_NOERRORCODE 31

IRQ 0, 32
IRQ 1, 33
IRQ 2, 34
IRQ 3, 35
IRQ 4, 36
IRQ 5, 37
IRQ 6, 38
IRQ 7, 39
IRQ 8, 40
IRQ 9, 41
IRQ 10, 42
IRQ 11, 43
IRQ 12, 44
IRQ 13, 45
IRQ 14, 46
IRQ 15, 47

[EXTERN isr_handler] ; Our C isr handler

isr_common_stub: ; Common handler
	pushad ; Push *ALL* the things! (Actually just the extended registers though).
	
	mov ax, ds ; lower bit of eax = ds
	push eax ; push the data segment

	; load the kernel data segment descriptor
	mov ax, 0x10
	mov ds, ax
	mov es, ax 
	mov fs, ax
	mov gs, ax
	
	call isr_handler
	
	; reload the kernel data segment descriptor
	pop ebx
	mov ds, bx
	mov es, bx
	mov fs, bx
	mov gs, bx

	popad ; pop *all* the extended registers!
	add esp, 8 ; this does some magic.
	iret ; i return. (actually: pops cs, ss, eip, eflags and esp).
	

[EXTERN irq_handler]

irq_common_stub:
	pushad ; Push *ALL* the things! (Actually just the extended registers though).
	
	mov ax, ds ; Lower bit of eax = ds
	push eax ; Push the data segment

	; Load the kernel data segment descriptor
	mov ax, 0x10
	mov ds, ax
	mov es, ax 
	mov fs, ax
	mov gs, ax
	
	call irq_handler
	
	; Reload the kernel data segment descriptor
	pop ebx
	mov ds, bx
	mov es, bx
	mov fs, bx
	mov gs, bx

	popad ; Pop *ALL* the extended registers!
	add esp, 8 ; This does some magic.
	sti
	iret ; I return. (Actually: pops CS, SS, EIP, EFLAGS and ESP).
		
