#ifndef MEMSET_H
#define MEMSET_H

void *memset(void *ptr, int v, size_t n);

#endif
