#include <stdint.h>
#include <stddef.h>

void *memset(void *ptr, int v, size_t n) {
	for (size_t i = 0; i < n; i++) {
		((char *) ptr)[i] = v;
	}
	return ptr;
}
