#ifndef OUTPUT_H
#define OUTPUT_H

#include <stdint.h>
#include <stddef.h>

#include "colors.h"

extern const size_t WIDTH;
extern const size_t HEIGHT;

uint8_t makeColor(Color_t, Color_t);

void putCharAt(char, uint8_t, size_t, size_t);

void putc(char);

void puts(const char*);

void setDefaultColor(Color_t, Color_t);

uint16_t terminalCharacter(char, uint8_t);

size_t getBufferIndex(size_t x, size_t y);

void clearScreen();

void initTerminal();

size_t strlen(const char*);

void puts_color(const char*, uint8_t);

void putc_color(char, uint8_t);

void printDec(uint32_t);

void setCursor(size_t, size_t);

void shiftUp();

#endif
