#include <stddef.h>
#include <stdint.h>

#include "output.h"
#include "colors.h"

const size_t WIDTH = 80;
const size_t HEIGHT = 25;

uint8_t defaultColor;
uint16_t* terminalBuffer;

size_t cursorX = 0;
size_t cursorY = 0;

uint8_t makeColor(Color_t fg, Color_t bg) {
	return fg | bg << 4;
}

void setDefaultColor(Color_t fg, Color_t bg) {
	defaultColor = makeColor(fg, bg);
}

uint16_t terminalCharacter(char c, uint8_t color) {
	uint16_t c16 = c;
	uint16_t color16 = color;
	return c16 | color16 << 8;
}

size_t getBufferIndex(size_t x, size_t y) {
	return y * WIDTH + x;
}

void putCharAt(char c, uint8_t color, size_t x, size_t y){
	size_t index = getBufferIndex(x, y);
	size_t termChar = terminalCharacter(c, color);
	terminalBuffer[index] = termChar;
}

void putc(char c) {
	putc_color(c, defaultColor);
}

void putc_color(char c, uint8_t color) {
	// Check if we need to shift.
	if (cursorY >= HEIGHT) {
		cursorY = HEIGHT - 1;
		shiftUp();
	}

	if (c == '\n') {
		for (size_t i = cursorX; i<WIDTH; i++) {
			putCharAt(' ', color, i, cursorY);
		}
		cursorY++;
		cursorX = 0;
	} else {
		putCharAt(c, color, cursorX, cursorY);
		cursorX++;
	}

	// Check if we need to wrap.
	if (cursorX >= WIDTH) {
		cursorX = 0;
		cursorY++;
	}

	// TODO: Set the real life cursor.
}

void puts(const char* str) {
	puts_color(str, defaultColor);
}

void clearScreen() {
	for ( size_t y = 0; y< HEIGHT; y++) {
		for (size_t x = 0; x< WIDTH; x++) {
			putCharAt(' ', defaultColor, x, y);
		}
	}
}


void initTerminal() {
	terminalBuffer = (uint16_t*) 0xB8000;
	setDefaultColor(WHITE, BLACK);
	clearScreen();

}

size_t strlen(const char* str) {
	size_t index = 0;
	while (1) {
		if (str[index] == '\0') {
			return index;
		}
		index++;
	}
	return -1;
}

void puts_color(const char* str, uint8_t color) {
	size_t length = strlen(str);
	for (size_t i = 0; i < length; i++) {
		putc_color(str[i], color);
	}	
}

void printDec(uint32_t n) {
	uint32_t mod_value = 1000000000;
	uint32_t minus_value;
	int character_index;
	int initial_zeroes = 1; // This is a boolean
	for (uint32_t i = 0; i < 10; i++) {
		minus_value = n % mod_value;
		character_index = (n-minus_value) / mod_value;
		char c = '0' + character_index;
		if (c == '0') {
			if (!initial_zeroes) {
				putc(c);	
			}
		} else {
			initial_zeroes = 0;
			putc(c);
		}
		n -= n-minus_value;
		mod_value = mod_value/10;
	}
}



void setCursor(size_t x, size_t y) {
	cursorX = x;
	cursorY = y;
}

void shiftUp() {
	for (size_t row=1; row<HEIGHT; row++) {
		for (size_t col=0; col<WIDTH; col++) {
			size_t oldLocation = getBufferIndex(col, row);
			size_t newLocation = getBufferIndex(col, row-1);
			terminalBuffer[newLocation] = terminalBuffer[oldLocation];
		}
	}

	// Zero the newly created line at the end.
	for (size_t i=0; i<WIDTH; i++) {
		putCharAt(' ', defaultColor, i, HEIGHT-1);
	}
}
