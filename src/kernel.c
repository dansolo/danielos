#include <stddef.h>
#include <stdint.h>

#include "vga/colors.h"
#include "vga/output.h"
#include "interrupts/idt.h"
#include "input/input.h"
#include "timer/timer.h"
#include "io/io.h"

void kernel_main() {
	init_idt();
	initTerminal();
	init_keyboard();
	init_timer(1);
	puts("Kernel initialised.");

	timer_start();

	while (1) {
		if (timer_tick % 20 == 0) {
			puts("Ding!");
			printDec(timer_tick);
			puts("\n");
		}
	}
}

