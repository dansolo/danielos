#include <stdint.h>

#include "../vga/output.h"
#include "../interrupts/isr.h"
#include "../io/io.h"

static void keyboard_callback(registers_t regs) {
	(void) regs; // No I totally use regs, gcc.
	puts("A key event happened!\n");
	inb(0x60); // Read scancode so it is happy, and gives repeat business.
	//putc(c);
}

void init_keyboard() {
	register_interrupt_handler(IRQ1, &keyboard_callback);
}
