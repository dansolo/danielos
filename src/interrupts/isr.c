#include <stdint.h>
#include <stddef.h>

#include "isr.h"
#include "../vga/output.h"
#include "../vga/colors.h"
#include "../io/io.h"

isr_t interrupt_handlers[256];

void isr_handler(registers_t  regs) {
	(void) regs;
	setCursor(0, 0);
	puts_color("Recieved an unhandled interrupt.\n", RED);	
	puts("Number: ");
	printDec(regs.int_no);
	puts("\n");
}

void irq_handler(registers_t regs) {
	if (regs.int_no >= 40) { // This interrupt involves the slave PIC
		outb(0xA0, 0x20); // Send reset signal to the slave
	}

	// Send reset signal to the master
	outb(0x20, 0x20);

	if (interrupt_handlers[regs.int_no] != 0) {
		isr_t handler = interrupt_handlers[regs.int_no];
		handler(regs);
	}

}

void register_interrupt_handler(uint8_t n, isr_t handler) {
	interrupt_handlers[n] = handler;
}

