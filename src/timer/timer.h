#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>

extern uint32_t timer_tick;

void init_timer(uint32_t freq);

void timer_start();

void timer_stop();

void timer_reset();

#endif
