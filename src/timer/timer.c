#include <stdint.h>
#include <stdbool.h>

#include "timer.h"
#include "../interrupts/isr.h"
#include "../vga/output.h"
#include "../io/io.h"

bool timer_running = false;
uint32_t timer_tick = 0;

static void timer_callback(registers_t regs) {
	(void) regs; // Totally using this.
	if (timer_running) {
		timer_tick++;
	}
}

void init_timer(uint32_t freq) {
	timer_tick = 0;

	register_interrupt_handler(IRQ0, &timer_callback);

	uint32_t divisor = 1193180 / freq;

	outb(0x43, 0x36);

	uint8_t l = (uint8_t)(divisor & 0xFF);
	uint8_t h = (uint8_t)( (divisor>>8) & 0xFF );

	outb(0x40, l);
	outb(0x40, h);
}

void timer_stop() {
	timer_running = false;
}

void timer_start() {
	timer_running = true;
}


void timer_reset() {
	timer_tick = 0;
}
