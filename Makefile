CFLAGS = -std=gnu99 -ffreestanding -Wall -Wextra -pedantic -Werror
LDFLAGS = -ffreestanding -nostdlib -lgcc
ASFLAGS = -felf

CC = i686-elf-gcc
LD = i686-elf-gcc
AS = nasm

BUILDDIR = build

SOURCES = $(shell find src -name '*.c')
ASMSOURCES = $(shell find src/asm -name '*.nasm')
OBJECTS = $(addprefix $(BUILDDIR)/,$(SOURCES:%.c=%.o))
ASMOBJECTS = $(addprefix $(BUILDDIR)/,$(ASMSOURCES:%.nasm=%.o))

default: kernel.bin
	
.PHONY: default clean

kernel.bin: $(OBJECTS) $(ASMOBJECTS) src/linker.ld
	$(LD) $(LDFLAGS) -T src/linker.ld $(OBJECTS) $(ASMOBJECTS) -o $@

$(BUILDDIR)/%.o: %.nasm
	$(AS) $(ASFLAGS) $< -o $@

$(BUILDDIR)/%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	find build -name "*.o" | xargs rm	
